package dotsandboxes;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assumptions.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DotsAndBoxesGridTest {
    /*
     * Because Test classes are classes, they can have fields, and can have static fields.
     * This field is a logger. Loggers are like a more advanced println, for writing messages out to the console or a log file.
     */
    private static final Logger logger = LogManager.getLogger(DotsAndBoxesGridTest.class);

    /*
     * Tests are functions that have an @Test annotation before them.
     * The typical format of a test is that it contains some code that does something, and then one
     * or more assertions to check that a condition holds.
     *
     * This is a dummy test just to show that the test suite itself runs
     */
    

    // FIXME: You need to write tests for the two known bugs in the code.
    @Test
    public void testExisitingHorizontalLineThrowsException() throws LineAlreadyDrawnException {
      DotsAndBoxesGrid dbg = new DotsAndBoxesGrid(4, 3, 2);
          dbg.drawHorizontal(0, 0, 0);
        assertThrows(LineAlreadyDrawnException.class, () -> {dbg.drawHorizontal(0, 0, 0);});
    }

    @Test
    public void testExisitingVerticalLineThrowsException() throws LineAlreadyDrawnException {
      DotsAndBoxesGrid dbg = new DotsAndBoxesGrid(4, 3, 2);
          dbg.drawVertical(0, 0, 0);
        assertThrows(LineAlreadyDrawnException.class, () -> {dbg.drawVertical(0, 0, 0);});
    }
 
    @Test
    public void testOutOfBoundsHorizontalLineThrowsException() {
      DotsAndBoxesGrid dbg = new DotsAndBoxesGrid(4, 3, 2);
         
        assertThrows(IndexOutOfBoundsException.class, () -> {dbg.drawHorizontal(4, 0, 0);});
    }



    @Test
    public void testBoxComplete() throws LineAlreadyDrawnException {
        DotsAndBoxesGrid dbg = new DotsAndBoxesGrid(4, 3, 2);
        assertEquals(dbg.boxComplete(0,0),false);
        dbg.drawHorizontal(0,0,0);
        assertEquals(dbg.boxComplete(0,0),false);
        dbg.drawVertical(0,0,0);
        assertEquals(dbg.boxComplete(0,0),false);
        dbg.drawVertical(1,0,0);
        assertEquals(dbg.boxComplete(0,0),false);
        dbg.drawHorizontal(0,1,0);
        assertEquals(dbg.boxComplete(0,0),true);

    }




}
